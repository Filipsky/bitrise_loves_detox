describe('Main View Test', () => {
  beforeEach(async () => {
    await device.reloadReactNative();
  });

  it('Hello Text is visible', async () => {
    await expect(element(by.id('app.main.hello'))).toBeVisible();
  });

  it('Hello Text value is Hello World', async () => {
    await expect(element(by.id('app.main.hello'))).toHaveText("Hello World")
  });
});
